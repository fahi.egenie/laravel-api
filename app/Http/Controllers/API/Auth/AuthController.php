<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Notifications\SignupActivate;
// use Illuminate\Support\Facades\Response;

class AuthController extends Controller
{
    
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'gender' => 'required',
            'password' => 'required|string|confirmed'
        ]);

        $profile_image = '';
        if ($request->hasFile('profile_image')) {
            $file = $request->profile_image;
            $folder = 'users';
            $profile_image = saveImages($file, $folder);
        }

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'profile_image' => $profile_image,
            'phone' => $request->phone,
            'country' => $request->country,
            'description' => $request->description,
            'gender' => $request->gender,
            'date_of_birth' => $request->date_of_birth,
            'status' => $request->status,
            'password' => bcrypt($request->password),
            'activation_token' => str_random(60)
        ]);

        $user->save();
        $user->notify(new SignupActivate($user));
        return response()->json([
            'message' => 'User has been registered successfully and Please confirm your email !'
        ], 201);
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
       
        if(Auth::attempt($credentials)){
            if(Auth::user()->active != 1 && Auth::user()->activation_token != ''){
                return response()->json([
                    'message' => 'Please confirm your email '
                ], 401);
            }
        }
        $credentials['active'] = 1;
        $credentials['deleted_at'] = null;

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ]);
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
    public function edit($id){
        $user = User::find($id);

        if(!$user){
            return response()->json([
                    'message' => 'User not found !'
                ], 401);
        }
        return response()->json($user,201);
    }
    public function update(Request $request,$id){
        $user = User::find($id);

        if(!$user){
            return response()->json([
                    'message' => 'User not found !'
                ], 401);
        }
      
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->description = $request->description;
        $user->gender = $request->gender;
        $user->date_of_birth = $request->date_of_birth;
        $user->status = $request->status;

        $user->save();
        return response()->json([
            'message' => 'Record updated successfully !'
        ],201);
    }
    
    public function updateProfileImage(Request $request,$id){
        
        if ($request->hasFile('profile_image')) {
            $user = User::find($id);
            $file = $request->profile_image;
            $folder = 'users';
            $user = User::find($id);
            $user->profile_image = saveImages($file, $folder);
            $user->save();

            return response()->json([
                'message' => 'Profile image update successfully !'
            ],201);
        }

        return response()->json([
                    'message' => 'File not exist , please try again!'
                ], 401);
    }
    
    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid.'
            ], 404);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->save();
        return $user;
    }


}
