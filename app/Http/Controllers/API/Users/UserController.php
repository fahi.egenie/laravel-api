<?php

namespace App\Http\Controllers\API\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    
    public function searchUser(Request $request){
    	$keyword = $request->get('keyword');
    	
		$users = User::where('name', 'like', "%{$keyword}%")
		                 ->orWhere('email', 'like', "%{$keyword}%")
		                 ->get();
		                 
		// return response()->json($request->user());
		return Response::json([
		    'data' => $users
		]);
    }
}
