<?php


// private function handle_video_file_upload($request)
//     {
//         $to_return = [];
//         if ($request->hasFile('video')) {

//             $video_file = $request->file('video');
//             $video_original_name = $video_file->getClientOriginalName();
//             $ext = $video_file->getClientOriginalExtension();

//             $temp_path = storage_path('temp');

//             $file_name = md5(time()) . ".stream." . $ext;
//             $wowza_path = base_path('wowza_store');

//             $output_file_name = md5(time()) . ".stream.mp4";

//             $video_path = $video_file->move($temp_path, $file_name);

//             copy($temp_path . DIRECTORY_SEPARATOR . $file_name, $wowza_path . DIRECTORY_SEPARATOR . $output_file_name);

//             ffmpeg_upload_file_path($video_path->getRealPath(), $wowza_path . DIRECTORY_SEPARATOR . $output_file_name);

//             $server = $this->getRandIp();

//             $stream_url = $this->make_streaming_server_url($server, $file_name, false);

//             $to_return = [
//                 'file_original_name' => $video_original_name,
//                 'file_name' => $output_file_name,
//                 'file_path' => $wowza_path . DIRECTORY_SEPARATOR . $output_file_name,
//                 'file_stream_url' => $stream_url,
//                 'file_server' => $server,
//             ];
//         }

//         return $to_return;
//     }

//     public function download(Request $request)
//     {
//         Log::log('info', 'download: ' . json_encode($request->all()));

//         $broadcast_id = $request->get('broadcast_id');

//         $file_name = $request->get('file_name');

//         $broadcast = Broadcast::find($broadcast_id);

//         if (!is_null($broadcast)) {
//             $file_name = $broadcast->filename;
//         }

//         $path = base_path('wowza_store' . DIRECTORY_SEPARATOR . $file_name);

//         if (is_file($path) && file_exists($path)) {
//             return response()->download($path, $file_name . '.mp4');
//         }
//     }

//     private function handle_image_file_upload($request, $broadcast_id, $user_id)
//     {
//         $image = $request->input('image');

//         $thumbnail_image = '';
//         if ($request->hasFile('image')) {
//             $file = $request->file('image');
//             $ext = $file->getClientOriginalExtension();
//             $thumbnail_image = md5(time()) . '.' . $ext;
//             $path = public_path('images' . DIRECTORY_SEPARATOR . 'broadcasts' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR);

//             if (!is_dir($path)) {
//                 mkdir($path);
//             }

//             $file->move($path, $thumbnail_image);

//             $this->fix_image_orientation($path . $thumbnail_image);

//             return $thumbnail_image;
//         }

//         if (!empty($image) && !is_null($image)) {
//             $thumbnail_image = md5(time()) . '.jpg';

//             $path = public_path('images' . DIRECTORY_SEPARATOR . 'broadcasts' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR);

//             if (!is_dir($path)) {
//                 mkdir($path);
//             }

//             $base_64_data = $request->input('image');

//             $base_64_data = str_replace('datagea:im/jpeg;base64,', '', $base_64_data);
//             $base_64_data = str_replace('data:image/png;base64,', '', $base_64_data);

//             File::put($path . $thumbnail_image, base64_decode($base_64_data));

//             $this->fix_image_orientation($path . $thumbnail_image);

//             return $thumbnail_image;
//         }

//         return $thumbnail_image;
//     }

//     private function fix_image_orientation($image_absolute_path)
//     {
//         $image = Image::make($image_absolute_path);

//         $image->orientate();

//         unlink($image_absolute_path);

//         $image->save($image_absolute_path);
//     }