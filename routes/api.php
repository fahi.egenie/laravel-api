<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'API\Auth\AuthController@login');
    Route::post('signup', 'API\Auth\AuthController@signup');
    Route::get('signup/activate/{token}', 'API\Auth\AuthController@signupActivate');
    // reset password routes
    Route::post('password/create', 'API\Auth\PasswordResetController@create');
    Route::get('password/find/{token}', 'API\Auth\PasswordResetController@find');
    Route::post('password/reset', 'API\Auth\PasswordResetController@reset');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'API\Auth\AuthController@logout');
        Route::get('user', 'API\Auth\AuthController@user');

        Route::get('edit/{id}', 'API\Auth\AuthController@edit');
        Route::put('update/{id}', 'API\Auth\AuthController@update');
        Route::post('update/profile/{id}','API\Auth\AuthController@updateProfileImage');
        Route::post('user/search','API\Users\UserController@searchUser');
    });
});



